pub fn set_panic_hook() {
    // When the `console_error_panic_hook` feature is enabled, we can call the
    // `set_panic_hook` function at least once during initialization, and then
    // we will get better error messages if our code ever panics.
    //
    // For more details see
    // https://github.com/rustwasm/console_error_panic_hook#readme
    #[cfg(feature = "console_error_panic_hook")]
    console_error_panic_hook::set_once();
}


pub fn hue2rgb(p: f32, q: f32, mut t: f32) -> f32
{
	if t < 0.0
        { t += 1.0; }
	if t > 1.0
        { t -= 1.0; }
	if 6.0 * t < 1.0
        { return p + (q - p) * 6.0 * t; }
	if 2.0 * t < 1.0
        { return q; }
	if 3.0 * t < 2.0
        { return p + (q - p) * (2.0 / 3.0 - t) * 6.0; }
	return p;
}

pub fn hsl2rgb(h: f32, s: f32, l: f32) -> (u8,u8,u8)
{
    let r: f32;
	let g: f32;
	let b: f32;
	let q: f32;
	let p: f32;

    if s == 0.0
	{
		r = l;
		g = l;
		b = l;
	}
	else
	{
		if l < 0.5
        {
			q = l * (1.0 + s);
        }
		else
        {
			q = l + s - l * s;
        }
		p = 2.0 * l - q;
		r = hue2rgb(p, q, h + 1.0 / 3.0);
		g = hue2rgb(p, q, h);
		b = hue2rgb(p, q, h - 1.0 / 3.0);
	}
	return (((r * 255.0).round() as u8) ,  ((g * 255.0).round() as u8) , ((b * 255.0).round() as u8));
}
