mod utils;

use wasm_bindgen::Clamped;
use web_sys::CanvasRenderingContext2d;
use web_sys::ImageData;
use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
extern {
    fn alert(s: &str);

    #[wasm_bindgen(js_namespace = console)]
    fn log(s: usize);
}

#[wasm_bindgen]
pub struct Vars {
    pub width: usize,
    pub height: usize,
    pub range: f32,
    pub hue: u8,
    pub iters: u8,
    pub juliax: f32,
    pub juliay: f32
}

#[wasm_bindgen]
impl Vars {

    #[wasm_bindgen(constructor)]
    pub fn new(height: usize, width: usize, hue: u8, iters: u8, range: f32) -> Vars
    {
         Vars { 
             height,
             width,
             hue,
             range,
             iters,
             juliax: 0.,
             juliay: 0.,
         }
    } 

}

#[wasm_bindgen]
pub fn draw(ctx: CanvasRenderingContext2d, opts: & mut Vars) -> Result<(), JsValue>
{
    opts.juliax = ft_map(opts.juliax as f32, 0., opts.width as f32,  -1., 1.);
    opts.juliay = ft_map(opts.juliay as f32, 0., opts.height as f32, -1., 1.);
    let mut buf = vec![0; opts.width * opts.height * 4];
    for i in 0..opts.height
    {
        for j in 0..opts.width
        {
            let color = get_color(i,j, &opts);
            put_pixel(&mut buf, i , j, color, opts.width);
        }
    }
    let image_data = ImageData::new_with_u8_clamped_array_and_sh(Clamped(&mut buf), opts.width as u32, opts.height as u32).unwrap();
    ctx.put_image_data(&image_data, 0., 0.)
}

fn put_pixel(image: &mut Vec<u8>, x:usize, y:usize, rgb: (u8, u8, u8), width: usize)
{
    let index = (y * width  + x) * 4;
    image[index] = rgb.0;
    image[index + 1] = rgb.1;
    image[index + 2] = rgb.2;
    image[index + 3] = 255;
}

fn ft_map(n: f32, f1: f32, t1: f32, f2: f32, t2: f32) -> f32
{
    f2 + (n - f1) * ((t2 - f2) / (t1 - f1))
}

fn ft_map0(n: f32, t1: f32, f2: f32, t2: f32) -> f32
{
    f2 + n * (t2- f2) / t1 
}

fn get_color(x:usize, y:usize, opts: &Vars) -> (u8, u8, u8) 
{
   let mut a = ft_map0(x as f32, opts.height as f32, -opts.range, opts.range); 
   let mut b = ft_map0(y as f32, opts.width as f32, -opts.range, opts.range); 
   
   let ca = opts.juliax;
   let cb = opts.juliay;
   let mut n = 0;
   while n < opts.iters
   {
        let tmp =  a * a - b * b + ca;
        b = 2. * a * b  + cb;
        a = tmp;
        if a * a + b * b > 4.
        {
            return utils::hsl2rgb(
                        (30.0 + (1.2 * n as f32).round()) 
                        / opts.hue as f32, 0.7, 0.5);
        }
        n += 1;
   }
   return (0,0,0);
}
