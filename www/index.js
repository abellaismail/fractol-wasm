import * as wasm from "wasm-app";

const canvas = document.getElementById('playground');
let color_btn = document.querySelector(".color-btn");
let anim_btn = document.querySelector(".anim-btn");
let localOpts = {
  anim: 0
}

const ctx = canvas.getContext('2d');
let opts = new wasm.Vars(600, 600, 50, 50, 2.0);

document.addEventListener('keyup', zoom_in);
color_btn.addEventListener('click', update_colors);
anim_btn.addEventListener('click', ()=> localOpts.anim = !localOpts.anim);
canvas.addEventListener('mousemove', update_julia);

wasm.draw(ctx, opts)

function draw() {
  wasm.draw(ctx, opts)
}

function update_colors() {
  if (opts.hue == 100)
    opts.hue = 10;
  else
    opts.hue += 10;
  wasm.draw(ctx, opts);
};

function update_julia(e) {
    if (localOpts.anim == 0)
      return ;
    opts.juliax = e.offsetX;
    opts.juliay = e.offsetY;
    draw();
}

function zoom_in(e) {
  if (e.code ==  "ArrowUp")
  {
    opts.range = (opts.range - opts.range * .1);
    wasm.draw(ctx, opts)
  }
  else if (e.code == "KeyC")
    update_colors()
  else if (e.code == "KeyA")
    localOpts.anim = !localOpts.anim
}
